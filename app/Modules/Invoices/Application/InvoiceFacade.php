<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application;

use Exception;
use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Api\Dto\InvoiceFrontDto;
use App\Modules\Invoices\Api\Entity\Invoice\Invoice;
use App\Modules\Invoices\Api\InvoiceFacadeInterface;
use App\Modules\Invoices\Api\Mappers\InvoiceMapper;
use App\Modules\Invoices\Api\ValueObjects\Invoice\InvoiceId;
use App\Modules\Invoices\Infrastructure\Exceptions\InvoiceNotFound;
use App\Modules\Invoices\Infrastructure\Repository\InvoiceRepositoryInterface;

final readonly class InvoiceFacade implements InvoiceFacadeInterface
{

    public function __construct(private InvoiceRepositoryInterface $repository)
    {
    }

    /**
     * @throws InvoiceNotFound
     * @throws Exception
     */
    public function find(InvoiceId $id): Invoice
    {
        $invoiceStdClass = $this->repository->find($id);
        return InvoiceMapper::fromDbToDomain($invoiceStdClass);
    }

    /**
     * @param InvoiceId $id
     * @return InvoiceFrontDto
     * @throws Exception
     */
    public function findWithRelations(InvoiceId $id): InvoiceFrontDto
    {
        $invoiceStdClass = $this->repository->findWithRelations($id);
        $products = $this->repository->getProducts($id);
        return InvoiceMapper::fromDbToFront($invoiceStdClass, $products);
    }

    /**
     */
    public function updateStatus(InvoiceId $id, StatusEnum $statusEnum): int
    {
        return $this->repository->updateStatus($id, $statusEnum);
    }
}
