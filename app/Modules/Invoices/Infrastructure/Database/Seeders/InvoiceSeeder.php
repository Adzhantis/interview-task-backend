<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Database\Seeders;

use App\Domain\Enums\StatusEnum;
use Exception;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class InvoiceSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run(): void
    {
        $companies = DB::table('companies')->get();
        $products = DB::table('products')->get();

        $faker = Factory::create();

        $invoices = [];

        for ($i = 0; $i < 10; $i++) {
            $invoices[] = [
                'id' => Uuid::uuid4()->toString(),
                'number' => $faker->uuid(),
                'date' => $faker->date(),
                'due_date' => $faker->date(),
                'company_id' => $companies->random()->id,
                'billed_company_id' => $companies->random()->id,
                'status' => StatusEnum::cases()[array_rand(StatusEnum::cases())],
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }

        DB::table('invoices')->insert($invoices);
        $this->addInvoiceProductLines($products, $invoices);
    }

    /**
     * @throws Exception
     */
    private function addInvoiceProductLines(Collection $products, array $invoices): void
    {

        $lines = [];

        foreach ($invoices ?? [] as $invoice) {
            $randomNumberOfProducts = random_int(1, 5);
            $freshProducts = clone $products;

            for ($i = 0; $i < $randomNumberOfProducts; $i++) {
                $lines[] = [
                    'id' => Uuid::uuid4()->toString(),
                    'invoice_id' => $invoice['id'],
                    'product_id' => $freshProducts->pop()->id,
                    'quantity' => random_int(1, 100),
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }
        }

        DB::table('invoice_product_lines')->insert($lines);
    }
}
