<?php

namespace App\Modules\Invoices\Infrastructure\Exceptions;


use Exception;
use Throwable;

class InvoiceNotUpdated extends Exception
{
    /**
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string     $message = "Invoice not updated",
        int        $code = 0,
        ?Throwable $previous = null
    )
    {
        parent::__construct($message, $code, $previous);
    }
}
