<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Repository;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Api\ValueObjects\Invoice\InvoiceId;
use App\Modules\Invoices\Infrastructure\Exceptions\InvoiceNotFound;
use App\Modules\Invoices\Infrastructure\Exceptions\InvoiceNotUpdated;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use stdClass;

final readonly class InvoiceRepository implements InvoiceRepositoryInterface
{
    /**
     * @param InvoiceId $id
     * @return stdClass
     * @throws InvoiceNotFound
     * @throws Exception
     */
    public function find(InvoiceId $id): stdClass
    {
        $invoice = DB::table('invoices')->find($id);

        if (!$invoice) {
            throw new InvoiceNotFound();
        }

        return $invoice;
    }

    /**
     * @param InvoiceId $id
     * @return stdClass
     * @throws InvoiceNotFound
     * @throws Exception
     */
    public function findWithRelations(InvoiceId $id): stdClass
    {

        $invoice = DB::table('invoices as i')
            ->select(
                'i.number',
                'i.date',
                'i.due_date',
                'c.name as company_name',
                'c.street as company_street',
                'c.city as company_city',
                'c.zip as company_zip',
                'c.phone as company_phone',

                'bc.name as billed_company_name',
                'bc.street as billed_company_street',
                'bc.city as billed_company_city',
                'bc.zip as billed_company_zip',
                'bc.phone as billed_company_phone',
                'bc.email as billed_company_email',
            )
            ->join('companies as c', 'i.company_id', '=', 'c.id')
            ->join('companies as bc', 'i.billed_company_id', '=', 'bc.id')
            ->where('i.id', '=', $id)
            ->first();

        if (!$invoice) {
            throw new InvoiceNotFound();
        }

        return $invoice;
    }

    public function getProducts(InvoiceId $id): Collection
    {
        return DB::table('products as p')
            ->join('invoice_product_lines as ipl', 'p.id', '=', 'ipl.product_id')
            ->where('ipl.invoice_id', '=', $id)
            ->get(['p.name', 'p.price', 'p.currency', 'ipl.quantity']);
    }

    /**
     * @param InvoiceId $id
     * @param StatusEnum $statusEnum
     * @return int
     * @throws InvoiceNotUpdated
     */
    public function updateStatus(InvoiceId $id, StatusEnum $statusEnum): int
    {
        $result = DB::table('invoices')
            ->where(['id' => $id])
            ->update([
                'status' => $statusEnum
            ]);

        if (!$result) {
            throw new InvoiceNotUpdated();
        }

        return $result;
    }
}
