<?php

namespace App\Modules\Invoices\Infrastructure\Repository;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Api\ValueObjects\Invoice\InvoiceId;
use Illuminate\Support\Collection;
use stdClass;

interface InvoiceRepositoryInterface
{
    /**
     * @param InvoiceId $id
     * @return stdClass
     */
    public function find(InvoiceId $id): stdClass;

    /**
     * @param InvoiceId $id
     * @return stdClass
     */
    public function findWithRelations(InvoiceId $id): stdClass;

    /**
     * @param InvoiceId $id
     * @return Collection
     */
    public function getProducts(InvoiceId $id): Collection;

    /**
     * @param InvoiceId $id
     * @param StatusEnum $statusEnum
     * @return int
     */
    public function updateStatus(InvoiceId $id, StatusEnum $statusEnum): int;
}
