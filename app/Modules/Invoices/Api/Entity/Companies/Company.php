<?php

namespace App\Modules\Invoices\Api\Entity\Companies;

use App\Modules\Invoices\Api\ValueObjects\Company\City;
use App\Modules\Invoices\Api\ValueObjects\Company\Name;
use App\Modules\Invoices\Api\ValueObjects\Company\Phone;
use App\Modules\Invoices\Api\ValueObjects\Company\Street;
use App\Modules\Invoices\Api\ValueObjects\Company\Zip;

final readonly class Company
{
    public function __construct(
        public Name   $name,
        public Street $street,
        public City   $city,
        public Zip    $zip,
        public Phone  $phone,
    )
    {
    }

}
