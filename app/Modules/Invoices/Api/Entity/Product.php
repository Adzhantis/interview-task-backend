<?php

namespace App\Modules\Invoices\Api\Entity;

use App\Modules\Invoices\Api\ValueObjects\InvoiceProductLine\Quantity;
use App\Modules\Invoices\Api\ValueObjects\Product\Total;
use App\Modules\Invoices\Api\ValueObjects\Product\Price;
use App\Modules\Invoices\Api\ValueObjects\Company\Name;

final readonly class Product
{
    public function __construct(
        public Name $name,
        public Quantity $quantity,
        public Price $price,
        public Total $total,
    )
    {
    }
}
