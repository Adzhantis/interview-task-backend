<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Entity\Invoice;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Api\ValueObjects\Invoice\BilledCompanyId;
use App\Modules\Invoices\Api\ValueObjects\Invoice\CompanyId;
use App\Modules\Invoices\Api\ValueObjects\Invoice\CreatedAt;
use App\Modules\Invoices\Api\ValueObjects\Invoice\Date;
use App\Modules\Invoices\Api\ValueObjects\Invoice\DueDate;
use App\Modules\Invoices\Api\ValueObjects\Invoice\InvoiceId;
use App\Modules\Invoices\Api\ValueObjects\Invoice\Number;
use App\Modules\Invoices\Api\ValueObjects\Invoice\UpdatedAt;

final readonly class Invoice implements InvoiceInterface
{
    public const ENTITY = 'Invoice';

    public function __construct(
        public InvoiceId       $id,
        public Number          $number,
        public Date            $date,
        public DueDate         $dueDate,
        public CompanyId       $companyId,
        public BilledCompanyId $billedCompanyId,
        public StatusEnum      $status,
        public CreatedAt       $createdAt,
        public UpdatedAt       $updatedAt,
    )
    {
    }
}
