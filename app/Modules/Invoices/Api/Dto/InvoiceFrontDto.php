<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Dto;

use App\Modules\Invoices\Api\Entity\Companies\BilledCompany;
use App\Modules\Invoices\Api\Entity\Companies\Company;
use App\Modules\Invoices\Api\ValueObjects\Invoice\Date;
use App\Modules\Invoices\Api\ValueObjects\Invoice\DueDate;
use App\Modules\Invoices\Api\ValueObjects\Invoice\Number;
use App\Modules\Invoices\Api\ValueObjects\Invoice\TotalPrice;
use Illuminate\Support\Collection;

final readonly class InvoiceFrontDto
{
    /**
     * @param Number $number
     * @param Date $date
     * @param DueDate $dueDate
     * @param Company $company
     * @param BilledCompany $billedCompany
     * @param Collection $products
     * @param TotalPrice $totalPrice
     */
    public function __construct(
        public Number $number,
        public Date $date,
        public DueDate $dueDate,
        public Company $company,
        public BilledCompany $billedCompany,
        public Collection $products,
        public TotalPrice $totalPrice,
    )
    {
    }
}
