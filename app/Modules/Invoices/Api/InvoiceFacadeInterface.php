<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Api\Dto\InvoiceFrontDto;
use App\Modules\Invoices\Api\Entity\Invoice\Invoice;
use App\Modules\Invoices\Api\ValueObjects\Invoice\InvoiceId;

interface InvoiceFacadeInterface
{
    public function find(InvoiceId $id): Invoice;

    public function findWithRelations(InvoiceId $id): InvoiceFrontDto;

    public function updateStatus(InvoiceId $id, StatusEnum $statusEnum): int;
}
