<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\ValueObjects\Invoice;

use App\Modules\Invoices\Api\ValueObjects\Abstract\StringObject;

final readonly class Number extends StringObject
{
    public function __construct(public string $number)
    {
        $this->validate($this->number);
    }
}
