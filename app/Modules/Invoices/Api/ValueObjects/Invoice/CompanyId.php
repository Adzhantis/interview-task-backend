<?php

namespace App\Modules\Invoices\Api\ValueObjects\Invoice;

use App\Modules\Invoices\Api\ValueObjects\Abstract\Id;
use Ramsey\Uuid\UuidInterface;

final class CompanyId extends Id
{
    public function __construct(public readonly UuidInterface $uuid)
    {
    }
}
