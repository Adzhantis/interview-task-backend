<?php

namespace App\Modules\Invoices\Api\ValueObjects\Invoice;

use Illuminate\Support\Collection;

final readonly class TotalPrice
{
    public int $totalPrice;

    public function __construct(Collection $products)
    {
        $sum = 0;
        foreach ($products->pluck('total') as $total) {
            $sum += $total();
        }

        $this->totalPrice = $sum;
    }
}
