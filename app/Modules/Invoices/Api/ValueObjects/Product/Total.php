<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\ValueObjects\Product;

use App\Modules\Invoices\Api\ValueObjects\InvoiceProductLine\Quantity;

final readonly class Total
{
    public int $total;

    public function __construct(private Quantity $quantity, private Price $price)
    {
        $this->total = $this->quantity->quantity * $this->price->price;
    }

    public function __invoke(): int
    {
        return $this->total;
    }
}
