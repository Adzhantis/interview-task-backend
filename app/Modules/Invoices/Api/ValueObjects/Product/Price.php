<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\ValueObjects\Product;

final readonly class Price
{
    public function __construct(public int $price)
    {

    }
}
