<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\ValueObjects\Company;

use App\Modules\Invoices\Api\ValueObjects\Abstract\StringObject;

final readonly class Email extends StringObject
{
    public function __construct(public string $email)
    {
        $this->validate($this->email);
    }
}
