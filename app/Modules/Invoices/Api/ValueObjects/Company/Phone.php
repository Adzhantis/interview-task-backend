<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\ValueObjects\Company;

use App\Modules\Invoices\Api\ValueObjects\Abstract\StringObject;

final readonly class Phone extends StringObject
{
    public function __construct(public string $phone)
    {
        $this->validate($this->phone);
    }
}
