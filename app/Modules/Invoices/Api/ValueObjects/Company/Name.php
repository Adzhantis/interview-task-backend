<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\ValueObjects\Company;

use App\Modules\Invoices\Api\ValueObjects\Abstract\StringObject;

final readonly class Name extends StringObject
{
    public function __construct(public string $name)
    {
        $this->validate($this->name);
    }
}
