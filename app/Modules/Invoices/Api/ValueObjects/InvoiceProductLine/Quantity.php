<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\ValueObjects\InvoiceProductLine;

final readonly class Quantity
{
    public function __construct(public int $quantity)
    {

    }
}
