<?php

namespace App\Modules\Invoices\Api\ValueObjects\Abstract;

use Ramsey\Uuid\Rfc4122\UuidV4;
use Ramsey\Uuid\UuidInterface;

abstract class Id
{
    public readonly UuidInterface $uuid;

    public function __toString(): string
    {
        return $this->uuid->toString();
    }

    public static function fromString(string $id): static
    {
        return new static(UuidV4::fromString($id));
    }


}
