<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\ValueObjects\Abstract;

use Illuminate\Database\Schema\Builder;
use LengthException;

abstract readonly class StringObject
{
    protected function validate(string $text): void
    {
        if (strlen(trim($text)) > Builder::$defaultStringLength) {
            throw new LengthException(sprintf('"%s" is too long', $text));
        }
    }
}
