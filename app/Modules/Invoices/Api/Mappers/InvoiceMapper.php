<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Mappers;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Api\Dto\InvoiceFrontDto;
use App\Modules\Invoices\Api\Entity\Companies\BilledCompany;
use App\Modules\Invoices\Api\Entity\Companies\Company;
use App\Modules\Invoices\Api\Entity\Invoice\Invoice;
use App\Modules\Invoices\Api\Entity\Product;
use App\Modules\Invoices\Api\ValueObjects\Company\City;
use App\Modules\Invoices\Api\ValueObjects\Company\Email;
use App\Modules\Invoices\Api\ValueObjects\Company\Name;
use App\Modules\Invoices\Api\ValueObjects\Company\Phone;
use App\Modules\Invoices\Api\ValueObjects\Company\Street;
use App\Modules\Invoices\Api\ValueObjects\Company\Zip;
use App\Modules\Invoices\Api\ValueObjects\Invoice\BilledCompanyId;
use App\Modules\Invoices\Api\ValueObjects\Invoice\CompanyId;
use App\Modules\Invoices\Api\ValueObjects\Invoice\CreatedAt;
use App\Modules\Invoices\Api\ValueObjects\Invoice\Date;
use App\Modules\Invoices\Api\ValueObjects\Invoice\DueDate;
use App\Modules\Invoices\Api\ValueObjects\Invoice\InvoiceId;
use App\Modules\Invoices\Api\ValueObjects\Invoice\Number;
use App\Modules\Invoices\Api\ValueObjects\Invoice\TotalPrice;
use App\Modules\Invoices\Api\ValueObjects\Invoice\UpdatedAt;
use App\Modules\Invoices\Api\ValueObjects\InvoiceProductLine\Quantity;
use App\Modules\Invoices\Api\ValueObjects\Product\Price;
use App\Modules\Invoices\Api\ValueObjects\Product\Total;
use Exception;
use Illuminate\Support\Collection;
use stdClass;

final readonly class InvoiceMapper
{
    /**
     * @throws Exception
     */
    public static function fromDbToDomain(stdClass $dbData): Invoice
    {
        return new Invoice(
            InvoiceId::fromString($dbData->id),
            new Number($dbData->number),
            new Date($dbData->date),
            new DueDate($dbData->due_date),
            CompanyId::fromString($dbData->company_id),
            BilledCompanyId::fromString($dbData->billed_company_id),
            StatusEnum::from($dbData->status),
            new CreatedAt($dbData->created_at),
            new UpdatedAt($dbData->updated_at),
        );
    }

    /**
     * @param stdClass $invoiceStdClass
     * @param Collection $products
     * @return InvoiceFrontDto
     * @throws Exception
     */
    public static function fromDbToFront(stdClass $invoiceStdClass, Collection $products): InvoiceFrontDto
    {
        $productsCollection = self::getProducts($products);

        return new InvoiceFrontDto(
            new Number($invoiceStdClass->number),
            new Date($invoiceStdClass->date),
            new DueDate($invoiceStdClass->due_date),
            new Company(
                new Name($invoiceStdClass->company_name),
                new Street($invoiceStdClass->company_street),
                new City($invoiceStdClass->company_city),
                new Zip($invoiceStdClass->company_zip),
                new Phone($invoiceStdClass->company_phone),
            ),
            new BilledCompany(
                new Name($invoiceStdClass->billed_company_name),
                new Street($invoiceStdClass->billed_company_street),
                new City($invoiceStdClass->billed_company_city),
                new Zip($invoiceStdClass->billed_company_zip),
                new Phone($invoiceStdClass->billed_company_phone),
                new Email($invoiceStdClass->billed_company_email),
            ),
            $productsCollection,
            new TotalPrice($productsCollection)
        );
    }

    /**
     * @param Collection $products
     * @return Collection
     */
    private static function getProducts(Collection $products): Collection
    {
        $productsCollection = new Collection();
        foreach ($products as $product) {
            $quantity = new Quantity($product->quantity);
            $price    = new Price($product->price);

            $productsCollection->add(new Product(
                new Name($product->name),
                $quantity,
                $price,
                new Total($quantity, $price)
            ));
        }

        return $productsCollection;
    }
}
