<?php

declare(strict_types=1);

namespace App\Infrastructure\Listeners;

use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\Events\EntityRejected;
use App\Modules\Invoices\Api\ValueObjects\Invoice\InvoiceId;
use App\Modules\Invoices\Infrastructure\Repository\InvoiceRepositoryInterface;

final readonly class InvoiceRejected
{
    public function __construct(
        public InvoiceRepositoryInterface $repository
    )
    {
    }

    /**
     * Handle the event.
     *
     * @param EntityRejected $event
     * @return void
     */
    public function handle(EntityRejected $event): void
    {
        $this->repository->updateStatus(new InvoiceId($event->approvalDto->id), StatusEnum::REJECTED);
    }
}

