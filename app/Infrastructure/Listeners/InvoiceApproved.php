<?php

declare(strict_types=1);

namespace App\Infrastructure\Listeners;

use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\Events\EntityApproved;
use App\Modules\Invoices\Api\ValueObjects\Invoice\InvoiceId;
use App\Modules\Invoices\Infrastructure\Repository\InvoiceRepositoryInterface;

final readonly class InvoiceApproved
{
    public function __construct(
        public InvoiceRepositoryInterface $repository
    )
    {
    }

    /**
     * Handle the event.
     *
     * @param EntityApproved $event
     * @return void
     */
    public function handle(EntityApproved $event): void
    {
        $this->repository->updateStatus(new InvoiceId($event->approvalDto->id), StatusEnum::APPROVED);
    }
}

