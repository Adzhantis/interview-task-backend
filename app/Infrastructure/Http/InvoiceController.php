<?php

declare(strict_types=1);

namespace App\Infrastructure\Http;

use App\Modules\Invoices\Api\ValueObjects\Invoice\InvoiceId;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Invoices\Api\Entity\Invoice\Invoice;
use App\Modules\Invoices\Api\InvoiceFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use Illuminate\Http\JsonResponse;
use Exception;

final readonly class InvoiceController
{
    public function __construct(
        public InvoiceFacadeInterface  $invoiceFacade,
        public ApprovalFacadeInterface $approvalFacade
    )
    {
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        try {
            $invoice = $this->invoiceFacade->findWithRelations(InvoiceId::fromString($id));
        } catch (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }

        return response()->json($invoice);
    }

    /**
     * @param string $id
     * @return JsonResponse
     */
    public function approve(string $id): JsonResponse
    {
        try {
            $invoice = $this->invoiceFacade->find(InvoiceId::fromString($id));
            $this->approvalFacade->approve(
                new ApprovalDto(
                    $invoice->id->uuid,
                    $invoice->status,
                    Invoice::ENTITY
                )
            );
        } catch (Exception $exception) {
            return response()->json([
                   'message' => $exception->getMessage()
                ], 500);
        }

        return response()->json([
            'message' => 'approval status is assigned'
        ]);
    }

    /**
     * @param string $id
     * @return JsonResponse
     */
    public function reject(string $id): JsonResponse
    {
        try {
            $invoice = $this->invoiceFacade->find(InvoiceId::fromString($id));
            $this->approvalFacade->reject(
                new ApprovalDto(
                    $invoice->id->uuid,
                    $invoice->status,
                    Invoice::ENTITY
                )
            );
        } catch (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }

        return response()->json([
            'message' => 'rejected status is assigned'
        ]);
    }
}
