<?php

namespace Tests;

use App\Domain\Enums\StatusEnum;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Testing\TestResponse;

/**
 *  php artisan test --testsuite=Feature
 *
 * Class TestCase
 * @package Tests
 * @author Igor Ilyev <phpcoreteam2@gmail.com>
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    private const BASE_URL = '/api/invoice';


    protected function getInvoiceByStatus(StatusEnum $status)
    {
        return DB::table('invoices')->where('status', $status)->first();
    }

    protected function getTestResponse($invoice, string $method): TestResponse
    {
        if ($method !== 'show') {
            $method = '/' . $method . '/';
        } else {
            $method = '/';
        }

        return $this->get(self::BASE_URL . $method . $invoice->id);
    }
}
