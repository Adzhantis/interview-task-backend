<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Domain\Enums\StatusEnum;
use Tests\TestCase;

class ApprovalTest extends TestCase
{
    use DatabaseMigrations;


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_already_approved()
    {
        $invoice = $this->getInvoiceByStatus(StatusEnum::APPROVED);

        $response = $this->getTestResponse($invoice, 'approve');

        $response->assertJson(['message' => 'approval status is already assigned']);
    }

    public function test_approve()
    {
        $invoice = $this->getInvoiceByStatus(StatusEnum::DRAFT);

        $response = $this->getTestResponse($invoice, 'approve');

        $response->assertJson(['message' => 'approval status is assigned']);
    }

}
