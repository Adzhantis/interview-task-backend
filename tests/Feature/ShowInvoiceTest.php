<?php

namespace Tests\Feature;

use App\Domain\Enums\StatusEnum;
use App\Infrastructure\Middleware\PreventRequestsDuringMaintenance;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class ShowInvoiceTest extends TestCase
{
    use DatabaseMigrations;

    public function test()
    {
        $invoice = $this->getInvoiceByStatus(StatusEnum::REJECTED);

        $response = $this->getTestResponse($invoice, 'show');

        $response->assertJson(['number' => [
            'number' => $invoice->number
        ]]);
    }
}
